# -*- coding: utf-8 -*-

import os.path
import sys
import datetime
import wx
import csv
import warnings
import traceback
import numpy as np
import astropy.units as u
import astropy.coordinates.name_resolve
from astropy.coordinates import SkyCoord, EarthLocation, Angle, get_constellation
from astropy.time import Time, TimeDelta
from astropy.utils import iers
from astropy.utils.exceptions import AstropyWarning
from astroplan import FixedTarget, Observer
from astroplan.plots import plot_airmass, plot_sky
from matplotlib.backends.backend_wxagg import (
    FigureCanvasWxAgg as FigureCanvas,
    NavigationToolbar2WxAgg as NavigationToolbar,
)
from matplotlib.figure import Figure

# On enlève les avertissements relatifs à iers dus à la fonction transform_to()
warnings.filterwarnings(
    action='ignore',
    category=AstropyWarning,
    module=r'.*astropy.coordinates',
)
warnings.filterwarnings(
    action='ignore',
    category=RuntimeWarning,
)

# Principe de base : pas d'accès à Internet
# iers.conf.auto_download = False
# iers.conf.auto_max_age = None

fichier_csv_base = 'base.csv'
fichier_csv_obs = 'obs.csv'
fichier_csv_cibles = 'cibles.csv'

couleur_resultat_defaut = wx.LIGHT_GREY
couleur_resultat_pickles = wx.YELLOW
couleur_resultat_miles = wx.GREEN
couleur_resultat_fond = wx.Colour(40, 40, 40)
couleur_erreur = wx.RED
couleur_normale = wx.GREEN


class FenetrePrincipale(wx.Frame):
    """ Unique fenêtre permettant la saisie des paramètres, et le calcul et la visualisation des résultats

    Tous les membres de la classe sont privés ou protégés.
    Pour alléger le code, on n'utilise pas la convention des '_' devant le nom des membres.
    """

    def __init__(self, titre, parametres):
        """ Constructeur et mise en page

        :param titre: titre principal de la fenêtre
        :param parametres : parametres passés sur la ligne de commande
        """
        super().__init__(None, title=titre)

        self.init_var(parametres)
        self.init_ihm()

    def init_var(self, parametres):
        # Initialisation des valeurs
        # -- Etoile cible (BL Cam par défault)
        self.nom_cible = 'BL Cam'
        self.heure_alpha = '3'
        self.minute_alpha = '47'
        self.degre_delta = '63'
        self.minute_delta = '22 '
        self.signe_delta = 1
        # -- Position observatoire
        self.nom_obs_defaut = "claix"
        self.degre_lat = '45'
        self.minute_lat = '07'
        self.signe_lat = 1
        self.degre_long = '5'
        self.minute_long = '40'
        self.signe_long = 1
        self.metre_alt = '310'
        # -- Temps de l'observation
        self.date = datetime.date.today()
        self.annee = str(self.date.year)
        self.mois = str(self.date.month)
        self.jour = str(self.date.day)
        self.heure = datetime.datetime.now(datetime.timezone.utc)
        self.heure_temps = str(self.heure.hour)
        self.minute_temps = str(self.heure.minute)
        # -- Bases de données
        # Parametres[0] contient le chemin d'accès au programme courant
        # Les fichiers de base de données se trouvent au même endroit, sauf celui des cibles
        rep = os.path.dirname(parametres[0])
        nom_csv_obs = os.path.join(rep, fichier_csv_obs)
        self.obs, self.nom_obs = self.lecture_fichier_csv(nom_csv_obs)
        self.nom_csv_base = os.path.join(rep, fichier_csv_base)
        self.fichier_csv_cibles = parametres[1]
        self.cibles, self.nom_cibles = self.lecture_fichier_csv(self.fichier_csv_cibles)

    def init_ihm(self):
        # Menu enfant
        filemenu = wx.Menu()
        self.menu_apropos = filemenu.Append(wx.ID_ABOUT, "&A Propos", " Informations sur ce programme")
        self.menu_sortie = filemenu.Append(wx.ID_EXIT, "E&xit", " Sortie")

        # Barre de menu minimaliste
        barre_menu = wx.MenuBar()
        barre_menu.Append(filemenu, '&Gestion')
        self.SetMenuBar(barre_menu)

        fenetres = wx.SplitterWindow(self, style=wx.SP_LIVE_UPDATE)

        self.panneau_saisie = wx.Panel(
            fenetres,
            name='Saisie',
            size=(600, -1),
            style=wx.BORDER_SUNKEN,
        )
        pl_saisie = self.init_zone_saisie()
        self.panneau_saisie.SetSizer(pl_saisie)
        # pl_saisie.Fit(self.panneau_saisie)

        self.panneau_graphique = wx.Panel(
            fenetres,
            name="Graphique",
            size=(600, 1200),
        )

        pl_graphique = self.init_zone_graphique()
        self.panneau_graphique.SetSizer(pl_graphique)

        fenetres.SplitVertically(self.panneau_saisie, self.panneau_graphique)
        pl_total = wx.BoxSizer(wx.HORIZONTAL)
        pl_total.Add(fenetres, 1, wx.ALL | wx.EXPAND, 1)

        # Ajustement des la taille réelle de l'objet wxFrame.
        #   Att : ne marche qu'avec les objets "top-level"
        self.SetSizer(pl_total)
        pl_total.Fit(self)

        self.affectation_evenements()

        self.Show(True)

    def init_zone_graphique(self):
        self.figure = Figure()
        self.graphique1 = self.figure.add_subplot(121)
        self.graphique2 = self.figure.add_subplot(122, projection='polar')

        canevas = FigureCanvas(self.panneau_graphique, -1, self.figure)

        # Barre d'outil Matplotlib
        barre_outil = NavigationToolbar(canevas)
        barre_outil.Realize()

        pl_graphique = wx.BoxSizer(wx.VERTICAL)
        pl_graphique.Add(canevas, 1, wx.LEFT | wx.TOP | wx.EXPAND)
        pl_graphique.Add(barre_outil, 0, wx.LEFT | wx.EXPAND)

        # maj des menus sur la barre d'outil
        barre_outil.update()

        return pl_graphique

    def init_zone_saisie(self):
        """ Dessine la partie de saisie manuelle. """

        pl_cible1, pl_cible2 = self.init_zone_cible()

        # Empilement vertical des zones de la cible
        pl_cible = wx.BoxSizer(wx.VERTICAL)
        pl_cible.AddMany([
            (pl_cible1, 0, wx.CENTER, 2),
            (pl_cible2, 0, wx.ALL | wx.EXPAND, 2),
        ])

        pl_site1, pl_site2 = self.init_zone_site()

        # Empilement vertical des zones de site
        pl_site = wx.BoxSizer(wx.VERTICAL)
        pl_site.AddMany([
            (pl_site1, 0, wx.CENTER, 2),
            (pl_site2, 0, wx.ALL | wx.EXPAND, 2),
        ])

        pl_date1, pl_date2 = self.init_zone_date()

        # Empilement vertical des zones de date
        pl_date = wx.BoxSizer(wx.VERTICAL)
        pl_date.AddMany([
            (pl_date1, 0, wx.CENTER),
            (pl_date2, 0, wx.ALL | wx.EXPAND, 2),
        ])

        pl_bouton = self.init_zone_boutons()

        pl_resultat = self.init_zone_resultats()

        pl_saisie = wx.BoxSizer(wx.VERTICAL)
        pl_saisie.AddMany([
            (pl_cible, 0, wx.ALL, 2),
            (pl_site, 0, wx.ALL, 2),
            (pl_date, 0, wx.ALL, 2),
            (pl_bouton, 0, wx.CENTER, 2),
            (pl_resultat, 1, wx.ALL, 2),
        ])

        return pl_saisie

    def init_zone_cible(self):
        """ Contient et reçoit les informations concernant la cible à étudier.

        Returns:
        tuple of horizontal BozSizer

        """

        # Champs pour l'étoile cible
        # 1 Etoile cible
        self.titre_etoile = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Etoile cible')
        self.edit_cible = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.nom_cible, style=wx.TE_PROCESS_ENTER)
        pl_cible1 = wx.BoxSizer(wx.HORIZONTAL)
        # Aucun des objets n'est étirable.
        pl_cible1.AddMany([(self.titre_etoile, 0, wx.ALL, 5), (self.edit_cible, 0, wx.ALL, 5)])

        # 1-1 Ascension droite
        self.etiq_alpha = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Asc. droite')
        self.etiq_heure_alpha = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Heures')
        self.edit_heure_alpha = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.heure_alpha, size=(3, 2))
        self.etiq_minute_alpha = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Minutes')
        self.edit_minute_alpha = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.minute_alpha, size=(3, 2))
        self.edit_heure_alpha.SetMaxLength(2)
        self.edit_minute_alpha.SetMaxLength(2)

        # 1-2 Déclinaison
        self.etiq_delta = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Déclinaison')
        self.etiq_degre_delta = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Degrés')
        self.edit_degre_delta = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.degre_delta, size=(3, 2))
        self.etiq_minute_delta = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Minutes')
        self.edit_minute_delta = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.minute_delta, size=(3, 2))
        self.br_signe_delta = wx.ComboBox(self.panneau_saisie, wx.ID_ANY, choices=['N', 'S'], style=wx.CB_DROPDOWN)
        self.br_signe_delta.SetSelection(int((1 - self.signe_delta) / 2))
        self.edit_degre_delta.SetMaxLength(2)
        self.edit_minute_delta.SetMaxLength(2)

        pl_cible2 = wx.GridSizer(2, 6, (2, 2))
        pl_cible2.AddMany([
            (self.etiq_alpha, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.etiq_heure_alpha, 0, wx.SHAPED, 1),
            (self.edit_heure_alpha, 0, wx.SHAPED, 1),
            (self.etiq_minute_alpha, 0, wx.SHAPED, 1),
            (self.edit_minute_alpha, 0, wx.SHAPED, 1),
            (wx.StaticText(self.panneau_saisie, wx.ID_ANY, ''), 1, wx.SHAPED, 1),
            (self.etiq_delta, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.etiq_degre_delta, 0, wx.SHAPED, 1),
            (self.edit_degre_delta, 0, wx.SHAPED, 1),
            (self.etiq_minute_delta, 0, wx.SHAPED, 1),
            (self.edit_minute_delta, 0, wx.SHAPED, 1),
            (self.br_signe_delta, 0, wx.SHAPED, 1),
        ])

        return (pl_cible1, pl_cible2)

    def init_zone_site(self):
        """ Contient et reçoit les information concernant le site d'observation.

        Returns:
        tuple of horizontal BozSizer

        """
        # 2 Site d'observation
        self.titre_site = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Site d\'observation')
        self.cb_site = wx.ComboBox(self.panneau_saisie, choices=self.nom_obs, style=wx.CB_DROPDOWN)
        self.cb_site.SetSelection(self.cb_site.FindString(self.nom_obs_defaut, caseSensitive=False))
        pl_site1 = wx.BoxSizer(wx.HORIZONTAL)
        # Aucun des objets n'est étirable.
        pl_site1.AddMany([(self.titre_site, 0, wx.ALL, 1), (self.cb_site, 0, wx.ALL, 1)])

        # 2-1 Latitude
        self.etiq_lat = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Latitude')
        self.etiq_degre_lat = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Degrés')
        self.edit_degre_lat = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.degre_lat, size=(3, 2))
        self.etiq_minute_lat = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Minutes')
        self.edit_minute_lat = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.minute_lat, size=(3, 2))
        self.br_signe_lat = wx.ComboBox(self.panneau_saisie, wx.ID_ANY, choices=['N', 'S'], style=wx.CB_DROPDOWN)

        # 2-2 Longitude
        self.etiq_long = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Longitude')
        self.etiq_degre_long = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Degrés')
        self.edit_degre_long = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.degre_long, size=(3, 2))
        self.etiq_minute_long = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Minutes')
        self.edit_minute_long = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.minute_long, size=(3, 2))
        self.br_signe_long = wx.ComboBox(self.panneau_saisie, wx.ID_ANY, choices=['E', 'O'], style=wx.CB_DROPDOWN)

        # 2-3 Altitude
        self.etiq_alt = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Altitude')
        self.etiq_metre_alt = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Mètres')
        self.edit_metre_alt = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.metre_alt, size=(3, 2))

        pl_site2 = wx.GridSizer(3, 6, (2, 2))
        pl_site2.AddMany([
            (self.etiq_lat, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.etiq_degre_lat, 0, wx.SHAPED, 1),
            (self.edit_degre_lat, 0, wx.SHAPED, 1),
            (self.etiq_minute_lat, 0, wx.SHAPED, 1),
            (self.edit_minute_lat, 0, wx.SHAPED, 1),
            (self.br_signe_lat, 0, wx.SHAPED),
            (self.etiq_long, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.etiq_degre_long, 0, wx.SHAPED, 1),
            (self.edit_degre_long, 0, wx.SHAPED, 1),
            (self.etiq_minute_long, 0, wx.SHAPED, 1),
            (self.edit_minute_long, 0, wx.SHAPED, 1),
            (self.br_signe_long, 0, wx.SHAPED),
            (self.etiq_alt, 0, wx.SHAPED, 1),
            (self.etiq_metre_alt, 0, wx.SHAPED, 1),
            (self.edit_metre_alt, 0, wx.SHAPED),
            (wx.StaticText(self.panneau_saisie, wx.ID_ANY, ''), 1, wx.SHAPED, 1),
            (wx.StaticText(self.panneau_saisie, wx.ID_ANY, ''), 1, wx.SHAPED, 1),
            (wx.StaticText(self.panneau_saisie, wx.ID_ANY, ''), 1, wx.SHAPED, 1),
        ])

        return (pl_site1, pl_site2)

    def init_zone_date(self):
        """ Contient et reçoit les informations concernant la date et l'heure de l'observation

        Returns:
        tuple of horizontal BozSizer

        """
        self.titre_temps = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Date et heure')
        pl_date1 = wx.BoxSizer(wx.HORIZONTAL)
        # Objet non-étirable
        pl_date1.Add(self.titre_temps, 0, wx.ALL, 1)

        # 3-1 Date
        self.etiq_annee = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Année')
        self.edit_annee = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.annee, size=(5, 2))
        self.etiq_mois = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Mois')
        self.edit_mois = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.mois, size=(3, 2))
        self.etiq_jour = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Jour')
        self.edit_jour = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.jour, size=(3, 2))

        # 3-2 Heure
        self.etiq_heure = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Heure TU')
        self.etiq_heure_temps = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Heures')
        self.edit_heure_temps = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.heure_temps, size=(3, 2))
        self.etiq_minute_temps = wx.StaticText(self.panneau_saisie, wx.ID_ANY, 'Minutes')
        self.edit_minute_temps = wx.TextCtrl(self.panneau_saisie, wx.ID_ANY, self.minute_temps, size=(3, 2))

        pl_date2 = wx.GridSizer(2, 6, (2, 2))
        pl_date2.AddMany([
            (self.etiq_annee, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.edit_annee, 0, wx.SHAPED, 1),
            (self.etiq_mois, 0, wx.SHAPED, 1),
            (self.edit_mois, 0, wx.SHAPED, 1),
            (self.etiq_jour, 0, wx.SHAPED, 1),
            (self.edit_jour, 0, wx.SHAPED, 1),
            (self.etiq_heure, 0, wx.RIGHT | wx.LEFT | wx.SHAPED, 5),
            (self.etiq_heure_temps, 0, wx.SHAPED, 1),
            (self.edit_heure_temps, 0, wx.SHAPED, 1),
            (self.etiq_minute_temps, 0, wx.SHAPED, 1),
            (self.edit_minute_temps, 0, wx.SHAPED, 1),
            (wx.StaticText(self.panneau_saisie, wx.ID_ANY, ''), 1, wx.SHAPED, 1),
        ])

        return (pl_date1, pl_date2)

    def init_zone_boutons(self):
        """ Contient les boutons d'action

        Returns:
        horizontal BozSizer
        """

        self.bouton_calcul = wx.Button(self.panneau_saisie, wx.ID_ANY, 'Calculer')
        self.bouton_arret = wx.Button(self.panneau_saisie, wx.ID_ANY, 'Fermer')

        pl_bouton = wx.BoxSizer(wx.HORIZONTAL)
        # Aucun étirement possible
        pl_bouton.AddMany([(self.bouton_calcul, 0, wx.ALIGN_LEFT, 5), (self.bouton_arret, 0, wx.ALIGN_RIGHT, 5)])
        return pl_bouton

    def init_zone_resultats(self):

        # Zone des résultats texte
        self.resultat = wx.TextCtrl(
            self.panneau_saisie,
            wx.ID_ANY,
            size=wx.Size(600, 400),
            style=wx.TE_MULTILINE | wx.TE_READONLY | wx.TE_DONTWRAP,
        )
        self.resultat.SetBackgroundColour(couleur_resultat_fond)
        self.resultat.SetForegroundColour(couleur_resultat_defaut)
        police_resultat = wx.Font(
            (0, 16),
            family=wx.FONTFAMILY_MODERN,
            style=wx.FONTSTYLE_NORMAL,
            weight=wx.FONTWEIGHT_NORMAL,
        )
        attribut = wx.TextAttr(
            couleur_resultat_defaut,
            colBack=couleur_resultat_fond,
            font=police_resultat,
        )
        self.resultat.SetDefaultStyle(attribut)

        pl_resultat = wx.BoxSizer(wx.VERTICAL)
        # Cette zone est étirable verticalement
        pl_resultat.Add(self.resultat, 1, wx.ALL, 2)
        return pl_resultat

    def affectation_evenements(self):
        """ Affecte un évènement à chacune des entrées susceptibles d'être modifiées
        """
        self.Bind(wx.EVT_MENU, self.sortie, self.menu_sortie)
        self.Bind(wx.EVT_MENU, self.a_propos, self.menu_apropos)
        self.Bind(wx.EVT_BUTTON, self.calcul, self.bouton_calcul)
        self.Bind(wx.EVT_BUTTON, self.sortie, self.bouton_arret)
        self.Bind(wx.EVT_TEXT_ENTER, self.lecture_cible, self.edit_cible)
        self.Bind(wx.EVT_TEXT, self.lecture_heure_alpha, self.edit_heure_alpha)
        self.Bind(wx.EVT_TEXT, self.lecture_minute_alpha, self.edit_minute_alpha)
        self.Bind(wx.EVT_TEXT, self.lecture_degre_delta, self.edit_degre_delta)
        self.Bind(wx.EVT_TEXT, self.lecture_minute_delta, self.edit_minute_delta)
        self.Bind(wx.EVT_RADIOBOX, self.lecture_signe_delta, self.br_signe_delta)
        self.Bind(wx.EVT_COMBOBOX, self.lecture_site, self.cb_site)
        self.Bind(wx.EVT_TEXT, self.lecture_degre_lat, self.edit_degre_lat)
        self.Bind(wx.EVT_TEXT, self.lecture_minute_lat, self.edit_minute_lat)
        self.Bind(wx.EVT_RADIOBOX, self.lecture_signe_lat, self.br_signe_lat)
        self.Bind(wx.EVT_TEXT, self.lecture_degre_long, self.edit_degre_long)
        self.Bind(wx.EVT_TEXT, self.lecture_minute_long, self.edit_minute_long)
        self.Bind(wx.EVT_RADIOBOX, self.lecture_signe_long, self.br_signe_long)
        self.Bind(wx.EVT_TEXT, self.lecture_metre_alt, self.edit_metre_alt)
        self.Bind(wx.EVT_TEXT, self.lecture_annee, self.edit_annee)
        self.Bind(wx.EVT_TEXT, self.lecture_mois, self.edit_mois)
        self.Bind(wx.EVT_TEXT, self.lecture_jour, self.edit_jour)
        self.Bind(wx.EVT_TEXT, self.lecture_heure_temps, self.edit_heure_temps)
        self.Bind(wx.EVT_TEXT, self.lecture_minute_temps, self.edit_minute_temps)

    def lecture_cible(self, evt):
        """ Lecture des coordonnées de la cible dans le fichier d'abord, puis via internet

        :param evt: évènement WX
        :return:
        """
        nom_cible = evt.GetString()
        coord = None
        try:
            # on cherche dans le fichier local
            cible = [element for element in self.cibles if element['Nom'].lower() == nom_cible.lower()][0]
            coord = SkyCoord(cible['Alpha'], cible['Delta'])
        except IndexError:
            # la cible n'a pas été trouvée dans le fichier
            # on essaie via internet
            try:
                cible = FixedTarget.from_name(nom_cible)
                coord = cible.coord
                alpha_delta = coord.to_string('hmsdms').split()
                self.cibles.append(dict([('Nom', nom_cible), ('Alpha', alpha_delta[0]), ('Delta', alpha_delta[1])]))
                self.ecriture_fichier_csv(self.fichier_csv_cibles)
                self.colore_champs_etoile(couleur_normale)
            except astropy.coordinates.name_resolve.NameResolveError:
                # la cible n'a nom plus pas été trouvée sur Simbad
                self.colore_champs_etoile(couleur_erreur)
                self.boite_erreur('Impossible de trouver {0}'.format(nom_cible))
        finally:
            if coord:
                self.edit_heure_alpha.SetValue(str(int(coord.ra.hms[0])))
                self.edit_minute_alpha.SetValue(str(int(coord.ra.hms[1] + coord.ra.signed_dms[2] / 60.0)))
                self.edit_degre_delta.SetValue(str(int(coord.dec.signed_dms[1])))
                self.edit_minute_delta.SetValue(str(int(coord.dec.signed_dms[2] + coord.dec.signed_dms[2] / 60.0)))
                self.br_signe_delta.SetSelection(int((1 - int(coord.dec.signed_dms[0]) / 2)))
                self.signe_delta = int(coord.dec.signed_dms[0])    # Pas d'évènement généré quand on change la sélection d'une radiobox
                self.edit_cible.SetValue(nom_cible)
                self.nom_cible = nom_cible
                self.bouton_calcul.Enable()
            else:
                self.bouton_calcul.Disable()

    def lecture_heure_alpha(self, evt):
        self.heure_alpha = evt.GetString()
        self.colore_champs_etoile(couleur_normale)
        self.edit_cible.Clear()

    def lecture_minute_alpha(self, evt):
        self.minute_alpha = evt.GetString()
        self.colore_champs_etoile(couleur_normale)
        self.edit_cible.Clear()

    def lecture_degre_delta(self, evt):
        self.degre_delta = evt.GetString()
        self.colore_champs_etoile(couleur_normale)
        self.edit_cible.Clear()

    def lecture_minute_delta(self, evt):
        self.minute_delta = evt.GetString()
        self.colore_champs_etoile(couleur_normale)
        self.edit_cible.Clear()

    def lecture_signe_delta(self, evt):
        self.signe_delta = -2 * evt.GetInt() + 1
        self.colore_champs_etoile(couleur_normale)
        self.edit_cible.Clear()

    def lecture_site(self, evt):
        nom_obs = evt.GetString()
        obs = [element for element in self.obs if element['Nom'] == nom_obs][0]
        if '---' not in obs['Nom']:
            coord = EarthLocation.from_geodetic(obs['Longitude'], obs['Latitude'], obs['Altitude'])
            self.edit_degre_lat.SetValue(str(int(coord.lat.signed_dms[1])))
            self.edit_minute_lat.SetValue(str(int(coord.lat.signed_dms[2] + coord.lat.signed_dms[3] / 60.0)))
            self.br_signe_lat.SetSelection(int((1 - int(coord.lat.signed_dms[0]) / 2)))
            self.signe_lat = int(coord.lat.signed_dms[0])    # Pas d'évènement généré quand on change la sélection d'une radiobox
            self.edit_degre_long.SetValue(str(int(coord.lon.signed_dms[1])))
            self.edit_minute_long.SetValue(str(int(coord.lon.signed_dms[2] + coord.lon.signed_dms[3] / 60.0)))
            self.br_signe_long.SetSelection(int((1 - int(coord.lon.signed_dms[0]) / 2)))
            self.signe_long = int(coord.lon.signed_dms[0])    # Pas d'évènement généré quand on change la sélection d'une radiobox
            self.edit_metre_alt.SetValue(str(int(coord.height.value + .5)))

    def lecture_degre_lat(self, evt):
        self.degre_lat = evt.GetString()

    def lecture_minute_lat(self, evt):
        self.minute_lat = evt.GetString()

    def lecture_signe_lat(self, evt):
        self.signe_lat = -2 * evt.GetInt() + 1

    def lecture_degre_long(self, evt):
        self.degre_long = evt.GetString()

    def lecture_minute_long(self, evt):
        self.minute_long = evt.GetString()

    def lecture_signe_long(self, evt):
        self.signe_long = -2 * evt.GetInt() + 1

    def lecture_metre_alt(self, evt):
        self.metre_alt = evt.GetString()

    def lecture_annee(self, evt):
        self.annee = evt.GetString()

    def lecture_mois(self, evt):
        self.mois = evt.GetString()

    def lecture_jour(self, evt):
        self.jour = evt.GetString()

    def lecture_heure_temps(self, evt):
        self.heure_temps = evt.GetString()

    def lecture_minute_temps(self, evt):
        self.minute_temps = evt.GetString()

    def colore_champs_etoile(self, couleur):
        self.edit_cible.SetForegroundColour(couleur)
        self.edit_heure_alpha.SetForegroundColour(couleur)
        self.edit_minute_alpha.SetForegroundColour(couleur)
        self.edit_degre_delta.SetForegroundColour(couleur)
        self.edit_minute_delta.SetForegroundColour(couleur)

    @staticmethod
    def lecture_fichier_csv(nom_fichier):
        """ Lecture d'un fichier csv contenant

        Le fichier doit avoir une 1ère ligne décrivant les champs. Le 1er champ doit être la chaine 'Nom'.
        :param nom_fichier: nom complet du fichier
        :return:
            - une liste contenant l'ensemble des noms.
            - une liste contenant l'ensemble de tous les champs (sous forme de liste par ligne)
        """
        try:
            with open(nom_fichier, newline='\n') as csvfile:
                nom_items = list()
                items = list()
                lecteur = csv.DictReader(csvfile, delimiter=',')
                for item in lecteur:
                    nom_items.append(item['Nom'])
                    items.append(item)
            return items, nom_items
        except EnvironmentError:
            return [], []

    def ecriture_fichier_csv(self, nom_fichier):
        with open(nom_fichier, 'w', newline='\n') as csvfile:
            ecrivain = csv.DictWriter(csvfile, fieldnames=['Nom', 'Alpha', 'Delta'])
            ecrivain.writeheader()
            ecrivain.writerows(self.cibles)

    def lecture_saisies(self):
        try:
            if int(self.metre_alt) < -400 or int(self.metre_alt) > 8840:
                self.boite_erreur('Altitude invraisemblable')
            else:
                # Récupère les valeurs saisies et détecte éventuellement des impossibilités
                # -- Etoile cible
                alpha = (float(self.heure_alpha) + float(self.minute_alpha) / 60.0) * 15.0
                delta = self.signe_delta * (float(self.degre_delta) + float(self.minute_delta) / 60.0)
                etoile_cible = FixedTarget(SkyCoord(alpha, delta, frame='icrs', unit='deg'), name=self.nom_cible)

                # -- Position observatoire
                latitude = Angle((self.signe_lat * int(self.degre_lat), int(self.minute_lat)), unit=u.deg)
                longitude = Angle((self.signe_long * int(self.degre_long), int(self.minute_long)), unit=u.deg)
                altitude = int(self.metre_alt)
                lieu_obs = EarthLocation.from_geodetic(longitude, latitude, altitude)

                # -- Temps de l'observation
                self.date = datetime.date(int(self.annee), int(self.mois), int(self.jour))
                self.heure = datetime.time(int(self.heure_temps), int(self.minute_temps))
                date_obs = '{0} {1}'.format(self.date, self.heure)

                return etoile_cible, lieu_obs, Time(date_obs)
        except Exception:
            self.boite_erreur(traceback.print_exc())

    def calcul(self, _):
        # Nettoyage de l'écran des résultats
        self.resultat.Clear()

        # Initialisation des calculs
        etoile, lieu_observatoire, date_obs = self.lecture_saisies()
        observatoire = Observer(location=lieu_observatoire, timezone="UTC")

        # Affichage textuel de diverses informations
        self.sortie_texte(observatoire, date_obs, etoile)

        # Affichage graphique des masses d'air et hauteur en fonction du temps
        self.sortie_graphique(observatoire, date_obs, etoile)

    def sortie_texte(self, obs, date_obs, etoile):
        altaz_etoile = obs.altaz(date_obs, etoile)
        temps_etoile_m1 = obs.target_meridian_transit_time(date_obs, etoile)
        altaz_etoile_m1 = obs.altaz(temps_etoile_m1, etoile)
        temps_etoile_m2 = obs.target_meridian_antitransit_time(date_obs, etoile)
        altaz_etoile_m2 = obs.altaz(temps_etoile_m2, etoile)
        anghor_etoile = obs.target_hour_angle(date_obs, etoile)
        if anghor_etoile > Angle(180 * u.deg):
            anghor_etoile = anghor_etoile - Angle(360 * u.deg)

        self.resultat.AppendText('Temps         : universel={0} / sidéral local={1}\n'.format(date_obs.to_value('iso', subfmt='date_hm'),
                                                                                              obs.local_sidereal_time(date_obs).to_string(fields=2, precision=0)))
        self.resultat.AppendText('\n')
        self.resultat.AppendText('Observatoire  : lat={0} / lon={1} / alt={2:.0f}\n'.format(obs.location.lat.to_string(fields=2, precision=0), obs.location.lon.to_string(fields=2, precision=0),
                                                                                            obs.location.height))
        self.resultat.AppendText('\n')
        self.resultat.AppendText('Cible         : {0} / coordonnées {1} ({2})\n'.format(etoile.name, etoile.coord.to_string(style='hmsdms', precision=0, fields=2),
                                                                                        get_constellation(etoile.coord, short_name=False)))
        self.resultat.AppendText('Cible         : azimuth={0} / hauteur={1}\n'.format(altaz_etoile.az.to_string(precision=0, fields=2), altaz_etoile.alt.to_string(precision=0, fields=2)))
        if obs.target_is_up(date_obs, etoile):
            self.resultat.AppendText('Cible         : angle horaire={0} / masse d\'air={1:.2f}\n'.format(anghor_etoile.to_string(unit=u.degree, precision=0, fields=1), altaz_etoile.secz))
        else:
            self.resultat.AppendText('Cible         : angle horaire={0} / non visible\n'.format(anghor_etoile.to_string(unit=u.degree, precision=0, fields=1)))
        self.resultat.AppendText('Cible         : lever={0} / coucher={1}\n'.format(
            obs.target_rise_time(date_obs, etoile).to_value('iso', subfmt='date_hm'),
            obs.target_set_time(date_obs, etoile).to_value('iso', subfmt='date_hm')))
        if obs.target_is_up(temps_etoile_m1, etoile):
            self.resultat.AppendText('Cible         : méridien      : date={0} / hauteur={1} / masse d\'air={2:.2f}\n'.format(temps_etoile_m1.to_value('iso', subfmt='date_hm'),
                                                                                                                              altaz_etoile_m1.alt.to_string(precision=0, fields=2),
                                                                                                                              altaz_etoile_m1.secz))
        else:
            self.resultat.AppendText('Cible         : méridien      : date={0} / hauteur={1} / non visible\n'.format(temps_etoile_m1.to_value('iso', subfmt='date_hm'),
                                                                                                                     altaz_etoile_m1.alt.to_string(precision=0, fields=2)))

        if obs.target_is_up(temps_etoile_m2, etoile):
            self.resultat.AppendText('Cible         : anti-méridien : date={0} / hauteur={1} / masse d\'air={2:.2f}\n'.format(temps_etoile_m2.to_value('iso', subfmt='date_hm'),
                                                                                                                              altaz_etoile_m2.alt.to_string(precision=0, fields=2),
                                                                                                                              altaz_etoile_m2.secz))
        else:
            self.resultat.AppendText('Cible         : anti-méridien : date={0} / hauteur={1} / non visible\n'.format(temps_etoile_m2.to_value('iso', subfmt='date_hm'),
                                                                                                                     altaz_etoile_m2.alt.to_string(precision=0, fields=2)))
        self.resultat.AppendText('\n')
        self.resultat.AppendText('Lune          : lever={0} / coucher={1}\n'.format(
            obs.moon_rise_time(date_obs).to_value('iso', subfmt='date_hm'),
            obs.moon_set_time(date_obs).to_value('iso', subfmt='date_hm')))
        self.resultat.AppendText('Lune          : illumination={0:.2f}\n'.format(obs.moon_illumination(date_obs)))
        self.resultat.AppendText('\n')
        self.resultat.AppendText('Soleil        : coucher={0} / lever={1}\n'.format(
            obs.sun_set_time(date_obs).to_value('iso', subfmt='date_hm'),
            obs.sun_rise_time(date_obs).to_value('iso', subfmt='date_hm')))
        self.resultat.AppendText('Soleil à -6°  : soir={0} / matin={1}\n'.format(
            obs.twilight_evening_civil(date_obs).to_value('iso', subfmt='date_hm'),
            obs.twilight_morning_civil(date_obs).to_value('iso', subfmt='date_hm')))
        self.resultat.AppendText('Soleil à -12° : soir={0} / matin={1}\n'.format(
            obs.twilight_evening_nautical(date_obs).to_value('iso', subfmt='date_hm'),
            obs.twilight_morning_nautical(date_obs).to_value('iso', subfmt='date_hm')))
        self.resultat.AppendText('Soleil à -18° : soir={0} / matin={1}\n'.format(
            obs.twilight_evening_astronomical(date_obs).to_value('iso', subfmt='date_hm'),
            obs.twilight_morning_astronomical(date_obs).to_value('iso', subfmt='date_hm')))

        self.resultat.AppendText('Soleil        : méridien={0} / anti-méridien={1}\n'.format(
            obs.midnight(date_obs).to_value('iso', subfmt='date_hm'),
            obs.noon(date_obs).to_value('iso', subfmt='date_hm')))
        self.resultat.Update()

        return

    def sortie_graphique(self, observatoire, date_obs, etoile):
        # matplotlib.rcParams['backend'] = 'Qt5Agg'

        # Intervalles de temps : du coucher au lever du soleil
        nuit = observatoire.sun_rise_time(date_obs) - observatoire.sun_set_time(date_obs) + TimeDelta(2 * u.hour)
        if nuit < 0:    # On considère le lever de soliel du jour suivant
            nuit = observatoire.sun_rise_time(date_obs + TimeDelta(24 * u.hour)) - observatoire.sun_set_time(date_obs) + TimeDelta(2 * u.hour)
        intervalle_temps_1 = observatoire.sun_set_time(date_obs) - TimeDelta(1 * u.hour) + np.linspace(0, nuit.to_value('sec'), 75) * u.s
        intervalle_temps_2 = observatoire.sun_set_time(date_obs) - TimeDelta(1 * u.hour) + np.linspace(0, nuit.to_value('sec'), 11) * u.s

        # Masse d'air
        plot_airmass(etoile, observatoire, intervalle_temps_1, ax=self.graphique1, altitude_yaxis=True, max_region=2.0, brightness_shading=True)
        self.graphique1.legend(shadow=True, loc='best')

        # Carte du ciel
        # Il faut obligatoirement que le graphique soit en mode polaire avant l'appel à plot_sky
        style = {'marker': '.'}
        plot_sky(etoile, observatoire, intervalle_temps_2, ax=self.graphique2, style_kwargs=style)
        self.graphique2.legend(shadow=True, loc='best')

    def a_propos(self, _):
        texte = 'Un outil pour afficher la masse d\'air et la position d\'une étoile'
        dlg = wx.MessageDialog(self, texte, 'Gaua', wx.OK)
        dlg.ShowModal()    # Mode bloquant
        dlg.Destroy()    #

    def boite_erreur(self, texte):
        dlg = wx.MessageDialog(self, texte, 'Erreur', wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def sortie(self, _):
        self.Close(True)    # Sortie propre


if __name__ == '__main__':
    app = wx.App(False)
    frame = FenetrePrincipale('Position Etoile', sys.argv)
    app.MainLoop()
